#!/usr/bin/env bash


# This file is part of comptools.

# comptools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# comptools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with comptools.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


trap 'exit 128' INT
export PATH


prog_name="$(basename "${0}")"
prog_desc="compile simple program source"
prog_args="FILES..."


usage() {
    cat <<EOF
Usage: ${prog_name} [OPTION]... ${prog_args}
${prog_name} - ${prog_desc}

Options:
    -V, --version  show program version
    -h, --help     show avalible options
EOF
}

version() {
    cat <<EOF
${prog_name} 1.1.3

Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
EOF
}


case "${1}"
in
    -h | -help | --help )
        usage
        exit 0
        ;;
    -V | -version | --version )
        version
        exit 0
        ;;
    -* )
        version
        echo
        usage
        exit 1
        ;;
esac


# Compiler options

CC="${CC:-gcc}"
CXX="${CXX:-g++}"

_CFLAGS=(
    -O2
    -Wall
    -Wunused
    -mtune=generic
    -pedantic
    -pipe
)

CFLAGS="${CFLAGS:-${_CFLAGS[@]}}"
CXXFLAGS="${CXXFLAGS:-${CFLAGS}}"

CC_cmd="${CC} ${CFLAGS}"
CXX_cmd="${CXX} ${CXXFLAGS}"


# Console output

# The exit status
EXIT_STATUS=0

# Colors
if command -v tput >/dev/null 2>&1
then
    blue="$(tput setb 0)$(tput bold)$(tput setaf 4)"
    green="$(tput setb 0)$(tput bold)$(tput setaf 2)"
    red="$(tput setb 0)$(tput bold)$(tput setaf 1)"
    reset="$(tput sgr0)"
fi


compile() {
    local my_cmd
    local my_bin

    if [ -d "${1}" ]
    then
        echo "File is a directory: ${1}"
        return 1
    elif [ ! -f "${1}" ]
    then
        echo "No such file: ${1}"
        return 1
    fi

    case "${1}" in
        *.c )
            my_cmd=${CC_cmd}
            ;;
        *.c++ | *.cpp | *.cxx )
            my_cmd=${CXX_cmd}
            ;;
        * )
            echo "No support for: ${1}"
            return 1
            ;;
    esac

    my_bin="$(basename "${1%.*}")"

    [ -f ./bin/"${my_bin}" ] && rm ./bin/"${my_bin}"

    echo "[${blue}CC${reset}] Compiling ${1} ..."

    if ${my_cmd} ./"${1}" -o ./bin/"${my_bin}"
    then
        echo "[${green}OK${reset}] SUCCESS compiling: ${1}"
        return 0
    else
        echo "[${red}!!${reset}] FAILURE compiling: ${1}"
        return 1
    fi
}


# Check command line arguments

if [ -z "${1}" ]
then
    echo "No arguments given"
    echo
    usage
    exit 1
fi


# Prepare

mkdir -p ./bin || exit 1

cat <<EOF

${green}----------------------------- ${reset}
${blue}Running with:                  ${reset}
  C   compiler = ${blue}${CC}         ${reset}
  CC  options  = ${blue}${CFLAGS}     ${reset}
  C++ compiler = ${blue}${CXX}        ${reset}
  CXX options  = ${blue}${CXXFLAGS}   ${reset}

EOF


# Compile

for f in "${@}"
do
    compile "${f}" || EXIT_STATUS=1
done

exit ${EXIT_STATUS}
