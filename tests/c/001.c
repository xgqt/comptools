/*
 * This file is part of comptools.

 * comptools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.

 * comptools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with comptools.  If not, see <https://www.gnu.org/licenses/>.

 * Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
 * Licensed under the GNU GPL v3 License
 */


#include <stdio.h>
#include <stdlib.h>


int main( )
{
    printf("Test 001");

    return 0;
}
