# COMP Tools

COMP Tools are simple scripts for compilation of simple C/C++ projects.
COMP Tools includes sane defaults for single-source-file projects.


# License

SPDX-License-Identifier: GPL-3.0-only

## Unless otherwise stated contents here are under the GNU GPL v3 license

This file is part of comptools.

comptools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

comptools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with comptools.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
